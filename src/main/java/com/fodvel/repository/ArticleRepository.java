package com.fodvel.repository;

import com.fodvel.domain.Article;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data MongoDB repository for the Article entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ArticleRepository extends MongoRepository<Article, String> {

	@Query(value = "{ 'type' : ?0}", fields="{content:0}")
	Page<Article> findAllExcludeContent(String type, Pageable pageable);

	@Query(value = "{}", fields="{content:0}")
	Page<Article> findAllExcludeContent(Pageable pageable);
}
