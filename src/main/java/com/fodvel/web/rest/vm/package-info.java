/**
 * View Models used by Spring MVC REST controllers.
 */
package com.fodvel.web.rest.vm;
