package com.fodvel.service.impl;

import com.fodvel.service.ArticleService;
import com.fodvel.domain.Article;
import com.fodvel.repository.ArticleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Article}.
 */
@Service
public class ArticleServiceImpl implements ArticleService {

    private final Logger log = LoggerFactory.getLogger(ArticleServiceImpl.class);

    private final ArticleRepository articleRepository;

    public ArticleServiceImpl(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    /**
     * Save a article.
     *
     * @param article the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Article save(Article article) {
        log.debug("Request to save Article : {}", article);
        return articleRepository.save(article);
    }

    /**
     * Get all the articles.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    public Page<Article> findAll(Pageable pageable) {
        log.debug("Request to get all Articles");
        return articleRepository.findAll(pageable);
    }


    /**
     * Get one article by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    public Optional<Article> findOne(String id) {
        log.debug("Request to get Article : {}", id);
        return articleRepository.findById(id);
    }

    /**
     * Delete the article by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Article : {}", id);
        articleRepository.deleteById(id);
    }

	@Override
	public Page<Article> findAllExcludeContent(Pageable pageable, String type) {
		// TODO Auto-generated method stub
		log.debug("Request to get all Articles exclude content");
		if(type == null || type.contentEquals("") || type.contentEquals("ALL")) {
	        return articleRepository.findAllExcludeContent(pageable);
		}else {
			return articleRepository.findAllExcludeContent(type, pageable);
		}
	}
}
