package com.fodvel.service;

import com.fodvel.domain.Article;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link Article}.
 */
public interface ArticleService {

    /**
     * Save a article.
     *
     * @param article the entity to save.
     * @return the persisted entity.
     */
    Article save(Article article);

    /**
     * Get all the articles.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Article> findAll(Pageable pageable);


    /**
     * Get all the articles.
     *
     * @param pageable the pagination information.
     * @param type 
     * @return the list of entities.
     */
    Page<Article> findAllExcludeContent(Pageable pageable, String type);

    
    /**
     * Get the "id" article.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Article> findOne(String id);

    /**
     * Delete the "id" article.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
}
